import requests, os, json

grafana_user = "admin"
grafana_password = "admin"

# [ provide credential variables etc ]
grafana_host = "65.108.205.60"
grafana_port = "3000"

grafana_url = f"http://{grafana_host}:{grafana_port}/"
session = requests.Session()
login_post = session.post(
    os.path.join(grafana_url, "login"),
    data=json.dumps({"user": grafana_user, "email": "", "password": grafana_password}),
    headers={"content-type": "application/json"},
)


data = json.dumps(
    {
        "id": 1,
        "uid": "F9mnhKRVk",
        "orgId": 1,
        "name": "Prometheus",
        "type": "prometheus",
        "typeLogoUrl": "",
        "access": "direct",
        "url": "http://65.108.205.60:9090",
        "password": "",
        "user": "",
        "database": "",
        "basicAuth": False,
        "basicAuthUser": "",
        "basicAuthPassword": "",
        "withCredentials": False,
        "isDefault": True,
        "jsonData": {"httpMethod": "POST"},
        "secureJsonFields": {},
        "version": 2,
        "readOnly": False,
    }
)

data_source = session.post(
    f"{grafana_url}api/datasources/", data, headers={"content-type": "application/json"}
)


f = open(os.path.join(".", "grafana", "panel.json"))
content = json.load(f)
data = {
    "dashboard": content,
}
data["folderId"] = 0
data["inputs"] = []
data["overwrite"] = True
panel_load = session.post(
    f"{grafana_url}api/dashboards/import",
    data=json.dumps(data),
    headers={"content-type": "application/json"},
)

print(data_source.content)
print(panel_load.content)
session.close()
