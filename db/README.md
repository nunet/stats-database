# Project Structure:
```
db
|___ docker-compose.yml
|___ Dockerfile
```

Run ```docker-compose up -d```

This is simply a docker container to run mongodb instance.

It holds the data processed by the data-processing module of the grpc server and makes it available to be used by exporter

