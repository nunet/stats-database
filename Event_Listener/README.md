# Project Structure:
```
Event_Listner
├───.dockerignore
├───docker-compose.yml
├───Dockerfile
├───requirements.txt
|
├───event_listener_spec
|   └───event_listener.proto
|
└───grpc_service
    ├───event_listener.py
    ├───test.py
    └───utils.py
```

Run ```docker-compose up -d```

**event_listener_spec/event_listener.proto** 
<p> It contains the prototype for the GRPC server. </p>

**grpc_service/event_listener.py** 
<p>It contains the code which runs the server. The GRPC server 
continuously listens for the events and processes them. Internally it calls the appropriate
method from Listner class from the **grpc_service/utils.py** and give the input data as parameter.</p>

**grpc_service/utils.py** 
<p>It ccepts the data and processes it accordingly bases on the event name 
and add/update/delete on the mongodb.</p>

**grpc_service/tet.py** 
<p>It is test script to simulate a call from the client to server with test data. </p>

