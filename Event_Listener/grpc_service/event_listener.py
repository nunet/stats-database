#!/usr/bin/env python3

from concurrent import futures
from time import sleep
import logging

import grpc
import event_listener_pb2
import event_listener_pb2_grpc
from utils import Listner, config

Listner = Listner()

GRPC_PORT = config.get("grpc_port", 50051)


if config.get("DEBUG") or config.get("TESTING"):
    import opentelemetry
    from opentelemetry import trace
    from opentelemetry.instrumentation.grpc import GrpcInstrumentorServer
    from opentelemetry.sdk.trace import TracerProvider
    from opentelemetry.exporter.jaeger.thrift import JaegerExporter
    from opentelemetry.sdk.trace.export import BatchSpanProcessor
    from opentelemetry.trace import set_span_in_context

    jaeger_address = "localhost"  # os.environ['jaeger_address']

    jaeger_exporter = JaegerExporter(
        agent_host_name=jaeger_address,
        agent_port=6831,
    )
    trace.set_tracer_provider(TracerProvider())
    trace_provider = trace.get_tracer_provider().add_span_processor(
        BatchSpanProcessor(jaeger_exporter)
    )

    grpc_server_instrumentor = GrpcInstrumentorServer()
    grpc_server_instrumentor.instrument()

    tracer_server = opentelemetry.instrumentation.grpc.server_interceptor(
        tracer_provider=trace_provider
    )
    tracer = trace.get_tracer(__name__)


class EventListener(event_listener_pb2_grpc.EventListenerServicer):
    def new_device_onboarded(self, request, context):
        if config.get("DEBUG") or config.get("TESTING"):
            current_span = trace.get_current_span()
            current_span.set_attribute("http.route", "new_device_onboarded")
            current_span.add_event("event message", {"device name": request.peer_id})

        res = event_listener_pb2.NewDeviceOnboardedOutput()
        res.peer_id, _ = Listner.new_device_onboarded(request)
        if config.get("DEBUG") or config.get("TESTING"):
            current_span.add_event("event message", {"result": str("object_id")})
        return res

    def device_status_change(self, request, context):
        if config.get("DEBUG") or config.get("TESTING"):
            current_span = trace.get_current_span()
            current_span.set_attribute("http.route", "device_status_change")
            current_span.add_event("event message", {"device name": request.peer_id})
        res = event_listener_pb2.DeviceStatusChangeOutput()
        res.peer_id, object_id = Listner.device_status_change(request)
        if config.get("DEBUG") or config.get("TESTING"):
            current_span.add_event("event message", {"result": str(object_id)})
        return res

    def device_resource_change(self, request, context):
        if config.get("DEBUG") or config.get("TESTING"):
            current_span = trace.get_current_span()
            current_span.set_attribute("http.route", "device_resource_change")
            current_span.add_event("event message", {"device name": request.peer_id})
        res = event_listener_pb2.DeviceResourceChangeOutput()
        res.peer_id, object_id = Listner.device_resource_change(request)
        if config.get("DEBUG") or config.get("TESTING"):
            current_span.add_event("event message", {"result": str(object_id)})
        return res

    def device_resource_config(self, request, context):
        if config.get("DEBUG") or config.get("TESTING"):
            current_span = trace.get_current_span()
            current_span.set_attribute("http.route", "device_resource_config")
            current_span.add_event("event message", {"device name": request.peer_id})
        res = event_listener_pb2.DeviceResourceConfigOutput()
        res.response, object_id = Listner.device_resource_config(request)
        if config.get("DEBUG") or config.get("TESTING"):
            current_span.add_event("event message", {"result": str(object_id)})
        return res

    def new_service(self, request, context):
        if config.get("DEBUG") or config.get("TESTING"):
            current_span = trace.get_current_span()
            current_span.set_attribute("http.route", "new_service")
            current_span.add_event("event message", {"device name": request.peer_id})
        res = event_listener_pb2.NewServiceOutput()
        res.response, object_id = Listner.new_service(request)
        if config.get("DEBUG") or config.get("TESTING"):
            current_span.add_event("event message", {"result": str(object_id)})
        return res

    def service_status(self, request, context):
        if config.get("DEBUG") or config.get("TESTING"):
            current_span = trace.get_current_span()
            current_span.set_attribute("http.route", "service_status")
            current_span.add_event("event message", {"device name": request.peer_id})
        res = event_listener_pb2.ServiceStatusOutput()
        res.response, object_id = Listner.service_status(request)
        if config.get("DEBUG") or config.get("TESTING"):
            current_span.add_event("event message", {"result": str(object_id)})
        return res

    def service_call(self, request, context):
        if config.get("DEBUG") or config.get("TESTING"):
            current_span = trace.get_current_span()
            current_span.set_attribute("http.route", "service_call")
            current_span.add_event("event message", {"device name": request.call_id})
        res = event_listener_pb2.ServiceCallOutput()
        res.response, object_id = Listner.service_call(request)
        if config.get("DEBUG") or config.get("TESTING"):
            current_span.add_event("event message", {"result": str(object_id)})
        return res

    def service_remove(self, request, context):
        if config.get("DEBUG") or config.get("TESTING"):
            current_span = trace.get_current_span()
            current_span.set_attribute("http.route", "service_call")
            current_span.add_event("event message", {"device name": request.peer_id})
        res = event_listener_pb2.ServiceRemoveOutput()
        res.response, object_id = Listner.service_remove(request)
        if config.get("DEBUG") or config.get("TESTING"):
            current_span.add_event("event message", {"result": str(object_id)})
        return res

    def ntx_payment(self, request, context):
        if config.get("DEBUG") or config.get("TESTING"):
            current_span = trace.get_current_span()
            current_span.set_attribute("http.route", "ntx_payment")
            current_span.add_event("event message", {"device name": request.peer_id})
        res = event_listener_pb2.NtxPaymentOutput()
        res.response, object_id = Listner.ntx_payment(request)
        if config.get("DEBUG") or config.get("TESTING"):
            current_span.add_event("event message", {"result": str(object_id)})
        return res


def main():
    grpc_port = GRPC_PORT
    print("Preparing to start")
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10000))
    event_listener_pb2_grpc.add_EventListenerServicer_to_server(EventListener(), server)
    server.add_insecure_port("[::]:" + str(grpc_port))
    server.start()
    print("Server listening on 0.0.0.0:{}".format(grpc_port))
    server.wait_for_termination()


if __name__ == "__main__":
    main()
