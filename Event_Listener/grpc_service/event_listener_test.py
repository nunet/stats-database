import random
import unittest
import grpc
import sys
import string
import event_listener_pb2
import event_listener_pb2_grpc
import random
manual_fun = None

if sys.argv.__len__() >= 2:
    manual_fun = sys.argv[1]

peer_id = "test-peer-id-#56808" + "".join(random.choices(string.ascii_lowercase, k=4))
call_id = float(random.random().__str__()[::-1])
service_id = "service-id-#56808" + "".join(random.choices(string.ascii_lowercase, k=4))

grpc_methods = [
    {
        "_fun": "failed_calls",
        "_klass": "DatabaseFailedCallInput",
        "_data": {"call_id": "45"},
        "response": "call_id",
    },
    {
        "_fun": "db_txn_agi",
        "_klass": "DatabaseAgiInput",
        "_data": {"call_id": "645", "txn_hash": "#DCSS3434"},
        "response": "txn_hash",
    },
    {
        "_fun": "db_add",
        "_klass": "DatabaseTelemetryInput",
        "_data": {
            "result": "success",
            "cpu_used": 23.4,
            "memory_used": 45.6,
            "net_used": 56.7,
            "time_taken": 12.1,
            "device_name": "developer-tester",
        },
        "response": "txn_hash",
    },
    {
        "_fun": "add_call",
        "_klass": "DatabaseAddCallInput",
        "_data": {
            "url": "www.google.com",
        },
        "response": "call_id",
    },
    {
        "_fun": "update_call",
        "_klass": "DatabaseUpdateCallInput",
        "_data": {"call_id": "1", "escrow_address": "www.google.com"},
        "response": "call_id",
    },
    {
        "_fun": "get_escrow",
        "_klass": "DatabaseGetEscrowInput",
        "_data": {
            "call_id": "1",
        },
        "response": "escrow_address",
    },
    {
        "_fun": "add_result",
        "_klass": "DatabaseAddResultInput",
        "_data": {
            "call_id": "1",
            "result": "success",
        },
        "response": "call_id",
    },
    {
        "_fun": "get_result",
        "_klass": "DatabaseGetResultInput",
        "_data": {
            "call_id": "1",
        },
        "response": "result",
    },
    {
        "_fun": "add_txn",
        "_klass": "DatabaseAddTransactionInput",
        "_data": {"call_id": "1", "txn_hash": "#DCSS3434"},
        "response": "call_id",
    },
    {
        "_fun": "get_txn",
        "_klass": "DatabaseGetTransactionInput",
        "_data": {
            "call_id": "1",
        },
        "response": "txn_hash",
    },
    {
        "_fun": "db_update_ntx",
        "_klass": "DatabaseNtxPaidInput",
        "_data": {
            "escrow_address": "www.google.com",
        },
        "response": "response",
    },
    {
        "_fun": "db_txn_agi",
        "_klass": "DatabaseAgiInput",
        "_data": {
            "txn_hash": "#DCSS3434",
            "call_id": "1",
            "service_name": "abc",
            "amount": "100",
        },
        "response": "response",
    },
    {
        "_fun": "failed_calls",
        "_klass": "DatabaseFailedCallInput",
        "_data": {
            "call_id": "1",
        },
        "response": "response",
    },
    {
        "_fun": "get_agi_txn",
        "_klass": "DatabaseGetTransactionInput",
        "_data": {
            "call_id": "1",
        },
        "response": "txn_hash",
    },
    {
        "_fun": "new_device_onboarded",
        "_klass": "NewDeviceOnboardedInput",
        "_data": {
            "peer_id": peer_id,
            "cpu": random.choice(range(4000,5000)),
            "ram": random.choice(range(2000,3000)),
            "network": random.choice(range(50,100)),
            "dedicated_time": random.choice(range(40)),
            "timestamp": random.choice(range(1000000, 23876559877)),
        },
        "response": "peer_id",
    },
    {
        "_fun": "device_status_change",
        "_klass": "DeviceStatusChangeInput",
        "_data": {"peer_id": peer_id, "status": "available", "timestamp": 23.12},
        "response": "peer_id",
    },
    {
        "_fun": "device_resource_change",
        "_klass": "DeviceResourceChangeInput",
        "_data": {
            "peer_id": peer_id,
            "changed_attribute_and_value": {
                "cpu": 200.0,
                "ram": 100.0,
                "network": 100.0,
                "dedicated_time": 100.0,
            },
            "timestamp": 23.12,
        },
        "response": "peer_id",
    },
    {
        "_fun": "device_resource_config",
        "_klass": "DeviceResourceConfigInput",
        "_data": {
            "peer_id": "test-peer",
            "changed_attribute_and_value": {
                "cpu": 2.2,
                "ram": 6.0,
                "network": 127.1,
                "dedicated_time": 5.5,
            },
            "timestamp": 23.12,
        },
        "response": "response",
    },
    {
        "_fun": "new_service",
        "_klass": "NewServiceInput",
        "_data": {
            "service_id": "test-service-id",
            "service_name": "service_name",
            "service_description": "service_description",
            "timestamp": 5.5,
        },
        "response": "response",
    },
    {
        "_fun": "service_call",
        "_klass": "ServiceCallInput",
        "_data": {
            'call_id': 33610308787356678415.0,
            'peer_id_of_service_host': "test-peer-id-#568088787ylzg", 
            'service_id': '1234567789', 
            'cpu_used': 1967, 
            'max_ram': 1285, 
            'memory_used': 1286, 
            'network_bw_used': 17, 
            'time_taken': 24, 
            'status': 'accepted', 
            'timestamp': 32809853417
        },
        "response": "response",
    },
    {
        "_fun": "service_status",
        "_klass": "ServiceStatusInput",
        "_data": {
            "call_id": 33610308787356678415.0,
            "service_id": "test-service-input-id",
            "peer_id_of_service_host": "test-peer-id-#568088787ylzg",
            "status": "finished without errors",
            "timestamp": 5.5,
        },
        "response": "response",
    },
    {
        "_fun": "service_remove",
        "_klass": "ServiceRemoveInput",
        "_data": {
            "service_id": "test-service-id",
            "timestamp": 2.2,
        },
        "response": "response",
    },
    {
        "_fun": "ntx_payment",
        "_klass": "NtxPaymentInput",
        "_data": {
            "service_id": "test-service-id",
            "amount_of_ntx": 2.2,
            "peer_id": "test-peer-id",
            "timestamp": 2.2,
        },
        "response": "response",
    },
]


def test_grpc():
    print("Running test script")
    default_mode = True
    if manual_fun:
        default_mode = False
    for grpc_event in grpc_methods:
        if (not default_mode) and (grpc_event.get("_fun", None) != manual_fun):
            continue
        with grpc.insecure_channel("127.0.0.1:50051") as channel:
            print(f"sending  {grpc_event.get('_fun')} event")
            stub = event_listener_pb2_grpc.EventListenerStub(channel)
            _stub = getattr(stub, grpc_event.get("_fun"))
            _stub_klass = getattr(event_listener_pb2, grpc_event.get("_klass"))
            print(_stub_klass)
            print(grpc_event.get("_data"))
            response = _stub(_stub_klass(**grpc_event.get("_data")))
        print("Event sent successfull response from server is", response.__str__())


test_grpc()
