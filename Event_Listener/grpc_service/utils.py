from pymongo import MongoClient
from datetime import datetime
from dotenv import dotenv_values

config = dotenv_values()

client = MongoClient(
    host=[
        f"mongodb://{config.get('mongo_username', 'mongodb_exporter')}:{config.get('mongo_password', 'password')}@{config.get('mongo_host', 'host.docker.internal')}:{config.get('mongo_port', '27017')}"
    ],
    serverSelectionTimeoutMS=3000,
)

STATUSMAP ={
    "accepted":1,
    "started":2,
    "finished without errors":3,
    "finished with errors":4,
    "withdraw with success": 5,
    "refund with success": 6,
    "withdraw with error": 7,
    "refund with error": 8,
    }


class MongoQuery:
    def __init__(self):
        self.client = client
        self.db = self.client[config.get("stats_db", "stats_db")]

    def insert_one(self, collection, data):
        self.db[collection].insert_one(data)

    def insert_many(self, collection, data):
        self.db[collection].insert_many(data)

    def replace_one(self, collection, filter, data):
        print("replace_one")
        print(collection, filter, data)
        self.db[collection].replace_one(filter, data, True)

    def update_one(self, collection, filter, data):
        self.db[collection].update_one(filter, data)

    def update_many(self, collection, filter, data):
        self.db[collection].update_many(filter, data)

    def find_one_and_delete(self, collection, filter, data):
        self.db[collection].find_one_and_delete(filter, data)

    def find_one_and_replace(self, collection, filter, data):
        self.db[collection].find_one_and_replace(filter, data)

    def find_one_and_update(self, collection, filter, data):
        self.db[collection].find_one_and_update(filter, data)

    def delete_one(self, collection, filter, data):
        self.db[collection].delete_one(filter, data)

    def delete_many(self, collection, filter, data={}):
        self.db[collection].delete_many(filter)

    def find_one(self, collection, filter={}):
        return self.db[collection].find_one(filter)

class ResourceTracker(MongoQuery):
    def monitor_manager(self, data, event_name):
        print('----Monitor Manager----')
        print(event_name)
        if event_name in [
            "new_device_onboarded",
            "device_status_change",
            "device_resource_change",
            "device_resource_config",
            "service_call",
        ]:
            if event_name in ["service_call"]:
                self.current_manager(data, event_name)
            self.total_manager(data, event_name)
        if event_name in ["new_device_onboarded", "device_resource_change", "device_resource_config"]:
            self.available_devices(data, event_name)

    def total_manager(self, data, event_name, action=None):
        if event_name in ["new_device_onboarded", "device_resource_change", "device_resource_config"]:
            self.total_manager_add(data, event_name)
        elif event_name == "device_status_change":
            self.available_devices(data, event_name)
            if data.get("status") in ["available", "ready"]:
                self.total_manager_add(data, event_name)
                self.current_manager_add(data, event_name)
            else:
                self.total_manager_remove(data, event_name)
                self.current_manager_remove(data, event_name)
        # if data.get("status") not in ["available", "ready"]:
        #     self.current_manager(data, event_name)

    def total_manager_add(self, data, event_name):
        self.action = "add"
        self._total(data, event_name)

    def total_manager_remove(self, data, event_name):
        self.action = "remove"
        self._total(data, event_name)

    def current_manager(self, data, event_name):
        print('----current_manager----')
        print(event_name)
        if event_name in ["device_resource_change", "service_call"]:
            self.action = "add"
            self.current_manager_add(data, event_name)

    def current_manager_add(self, data, event_name):
        print('----current_manager_add----')
        print(event_name)
        self.action = "add"
        self._current(data, event_name)

    def current_manager_remove(self, data, event_name):
        self.action = "remove"
        self._current(data, event_name)

    def _current(self, data, event_name):
        print('----_current----')
        print(event_name)
        self.current_cpu(data, event_name)
        self.current_ram(data, event_name)
        self.current_network(data, event_name)
        self.current_time(data, event_name)

    def current_cpu(self, data, event_name):
        peer_id = data.get("peer_id" if not event_name in ['service_call'] else "peer_id_of_service_host")
        cpu =  data.get("cpu" if not event_name in ['service_call'] else "cpu_used")
        if self.action == "add":
            print('----current_cpu----')
            print(event_name)
            self.replace_one(
                "current_cpu",
                {"peer_id": peer_id},
                {
                    "peer_id": peer_id,
                    "cpu": cpu,
                },
            )
        else:
            self.delete_many(
                "current_cpu",
                {"peer_id": peer_id},
            )

    def current_ram(self, data, event_name):
        peer_id = data.get("peer_id" if not event_name in ['service_call'] else "peer_id_of_service_host")
        ram =  data.get("ram" if not event_name in ['service_call'] else "ram_used")
        if self.action == "add":
            self.replace_one(
                "current_ram",
                {"peer_id": peer_id},
                {
                    "peer_id": peer_id,
                    "ram": data.get("ram"),
                },
            )
        else:
            self.delete_many(
                "current_ram",
                {"peer_id": peer_id},
            )

    def current_network(self, data, event_name):
        if self.action == "add":
            self.replace_one(
                "current_network",
                {"peer_id": data.get("peer_id")},
                {
                    "peer_id": data.get("peer_id"),
                    "network": data.get("network"),
                },
            )
        else:
            self.delete_many(
                "current_network",
                {"peer_id": data.get("peer_id")},
            )

    def current_time(self, data, event_name):
        if self.action == "add":
            self.replace_one(
                "current_dedicated_time",
                {"peer_id": data.get("peer_id")},
                {
                    "peer_id": data.get("peer_id"),
                    "dedicated_time": data.get("dedicated_time"),
                },
            )
        else:
            self.delete_many(
                "current_dedicated_time",
                {"peer_id": data.get("peer_id")},
            )

    def _total(self, data, event_name):
        self.total_cpu(data, event_name)
        self.total_ram(data, event_name)
        self.total_network(data, event_name)
        self.total_time(data, event_name)

    def total_cpu(self, data, event_name):
        if self.action == "add":
            self.replace_one(
                "total_cpu",
                {"peer_id": data.get("peer_id")},
                {
                    "peer_id": data.get("peer_id"),
                    "cpu": data.get("cpu"),
                },
            )
        elif self.action == "remove":
            self.delete_many(
                "total_cpu",
                {"peer_id": data.get("peer_id")},
            )

    def total_ram(self, data, event_name):
        if self.action == "add":
            self.replace_one(
                "total_ram",
                {"peer_id": data.get("peer_id")},
                {
                    "peer_id": data.get("peer_id"),
                    "ram": data.get("ram"),
                },
            )
        elif self.action == "remove":
            self.delete_many(
                "total_ram",
                {"peer_id": data.get("peer_id")},
            )

    def total_network(self, data, event_name):
        if self.action == "add":
            self.replace_one(
                "total_network",
                {"peer_id": data.get("peer_id")},
                {
                    "peer_id": data.get("peer_id"),
                    "network": data.get("network"),
                },
            )
        elif self.action == "remove":
            self.delete_many(
                "total_network",
                {"peer_id": data.get("peer_id")},
            )

    def total_time(self, data, event_name):
        if self.action == "add":
            self.replace_one(
                "total_dedicated_time",
                {"peer_id": data.get("peer_id")},
                {
                    "peer_id": data.get("peer_id"),
                    "dedicated_time": data.get("dedicated_time"),
                },
            )
        elif self.action == "remove":
            self.delete_many(
                "total_dedicated_time",
                {"peer_id": data.get("peer_id")},
            )

    def available_devices(self, data, event_name):
        if (
            data.get("status") in ["available", "ready"]
            or event_name in ["new_device_onboarded", "device_resource_change", "device_resource_config"]
        ):
            self.db["available_devices"].replace_one(
                {"peer_id": data.get("peer_id")},
                {
                    "peer_id": data.get("peer_id"),
                    "timestamp": data.get("timestamp"),
                },
                True,
            )
            self.replace_one("used_cpu", {"peer_id":"test-peer-id"}, {"peer_id":"test-peer-id","status":2,"cpu_used":0})
            self.replace_one("used_ram", {"peer_id":"test-peer-id"}, {"peer_id":"test-peer-id","status":2,"memory_used":0})
            self.replace_one("used_network_bw", {"peer_id":"test-peer-id"}, {"peer_id":"test-peer-id","status":2,"network_bw_used":0})
            self.replace_one("used_time", {"peer_id":"test-peer-id"}, {"peer_id":"test-peer-id","status":2,"time_taken":0})
        else:
            self.db["available_devices"].delete_many(
                {"peer_id": data.get("peer_id")},
            )


class Listner(ResourceTracker):
    def new_device_onboarded(self, data):
        collection_name = "new_device_onboarded"
        post_data = {
            "peer_id": data.peer_id,
            "cpu": data.cpu,
            "ram": data.ram,
            "network": data.network,
            "dedicated_time": data.dedicated_time,
            "timestamp": data.timestamp,
        }
        self.monitor_manager(post_data, collection_name)
        return (
            data.peer_id,
            data.peer_id,
        )

    def device_status_change(self, data):

        collection_name = "device_status_change"
        post_data = {
            "peer_id": data.peer_id,
            "status": data.status,
            "timestamp": data.timestamp,
        }
        self.monitor_manager(post_data, collection_name)
        return (
            data.peer_id,
            data.peer_id,
        )

    def device_resource_change(self, data):
        collection_name = "device_resource_change"
        post_data = {
            "peer_id": data.peer_id,
            "cpu": data.changed_attribute_and_value.cpu,
            "ram": data.changed_attribute_and_value.ram,
            "network": data.changed_attribute_and_value.network,
            "dedicated_time": data.changed_attribute_and_value.dedicated_time,
            "timestamp": data.timestamp,
        }
        self.monitor_manager(post_data, collection_name)
        return (
            data.peer_id,
            data.peer_id,
        )

    def device_resource_config(self, data):
        collection_name = "device_resource_config"
        post_data = {
            "peer_id": data.peer_id,
            "cpu": data.changed_attribute_and_value.cpu,
            "ram": data.changed_attribute_and_value.ram,
            "network": data.changed_attribute_and_value.network,
            "dedicated_time": data.changed_attribute_and_value.dedicated_time,
            "timestamp": data.timestamp,
        }
        self.monitor_manager(post_data, collection_name)
        return (
            data.peer_id,
            data.peer_id,
        )

    def new_service(self, data):
        collection_name = "new_service"
        post_data = {
            "service_id": data.service_id,
            "service_name": data.service_name,
            "service_description": data.service_description,
            "timestamp": data.timestamp,
        }
        return (
            data.service_id,
            self.db[collection_name].insert_one(post_data).inserted_id.__str__(),
        )

    def service_status(self, data):
        post_data = {
            "call_id": data.call_id,
            "peer_id_of_service_host": data.peer_id_of_service_host,
            "status": data.status,
            "timestamp": data.timestamp,
        }
        service_call = self.find_one(
            "service_call",
            { 'call_id': data.call_id, "peer_id_of_service_host":data.peer_id_of_service_host},
        )
        service_call.pop('_id',None)
        service_call['status'] = data.status
        service_call['active'] = True
        self.service_stats(service_call, data.peer_id_of_service_host, completed=True)
        return str(data.call_id), data.peer_id_of_service_host

    def service_stats(self, post_data, id, completed=False):
        status = STATUSMAP.get(post_data["status"].lower())
        status_str = post_data["status"].lower()
        if not status:
            return "failed wrong status", False
        else:
            post_data["status"] = status
        self.replace_one(
            "service_call",
            {"peer_id_of_service_host": id, "call_id": post_data["call_id"]},
            post_data,
        )
        self.update_services(post_data)
        if status_str in [ "started", "finished without errors", "finished with errors", "withdraw with success", "refund with success", "withdraw with error", "refund with error", 2,3,4,5,6,7,8]:
            self.replace_one(
                "used_cpu",
                {"call_id": post_data["call_id"]},
                post_data,
            )
            self.replace_one(
                "used_ram",
                {"call_id": post_data["call_id"]},
                post_data,
            )
            self.replace_one(
                "used_network_bw",
                {"call_id": post_data["call_id"]},
                post_data,
            )
            self.replace_one(
                "used_time",
                {"call_id": post_data["call_id"]},
                post_data,
            )

    def service_call(self, data):
        collection_name = "service_call"
        post_data = {
            "call_id": data.call_id,
            "peer_id_of_service_host": data.peer_id_of_service_host,
            "service_id": data.service_id,
            "cpu_used": data.cpu_used,
            "max_ram": data.max_ram,
            "memory_used": data.memory_used,
            "network_bw_used": data.network_bw_used,
            "time_taken": data.time_taken,
            "status": data.status,
            "amount_of_ntx": data.amount_of_ntx,
            "active": True,
            "timestamp": data.timestamp,
        }
        # self.monitor_manager(post_data, collection_name)
        self.service_stats(post_data, data.peer_id_of_service_host)
        return (
            data.peer_id_of_service_host,
            data.peer_id_of_service_host,
        )

    def service_remove(self, data):
        collection_name = "service_remove"
        post_data = {"service_id": data.service_id, "timestamp": data.timestamp}
        return (
            data.service_id,
            self.db[collection_name].insert_one(post_data).inserted_id.__str__(),
        )

    def ntx_payment(self, data):
        collection_name = "ntx_payment"
        post_data = {
            "call_id": data.call_id,
            "peer_id": data.peer_id,
            "service_id": data.service_id,
            "amount_of_ntx": data.amount_of_ntx,
            "success_fail_status": data.success_fail_status,
            "timestamp": data.timestamp,
        }
        self.update_one(
            "service_call",
            {"peer_id_of_service_host": data.peer_id, "call_id": data.call_id},
            {"$set":{"amount_of_ntx": data.amount_of_ntx}},
        )
        self.replace_one(
            collection_name,
            {"peer_id": data.peer_id, "call_id": data.call_id},
            post_data,
        )
        return (
            data.peer_id,
            data.peer_id,
        )

    def update_services(self, post_data):
        _filter = {"peer_id_of_service_host": {"$ne": post_data["peer_id_of_service_host"]}, "call_id": {"$ne": post_data["call_id"]}}
        data = {"$set": {"active": False} }
        self.update_many("service_call", _filter, data)
        self.update_many("used_cpu", _filter, data)
        self.update_many("used_ram", _filter, data)
        self.update_many("used_network_bw", _filter, data)
        self.update_many("used_time", _filter, data)
