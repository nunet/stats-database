# Script to start the services
xl=$(pwd)
echo $xl
cd $xl/db && docker-compose up --build -d
cd $xl/Event_Listener && docker-compose up --build -d
cd $xl/exporter && docker-compose up --build -d
cd $xl/grafana && docker-compose up --build -d
cd $xl/prometheus && docker-compose  up --build -d