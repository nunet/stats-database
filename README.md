# stats-database

Isolated code for the following component of the platform (fragment of [full architecture scheme](https://nunet.gitlab.io/architecture/architecture/corep)):

<img src="artifacts/place_in_architecture.png"  width="800">

# Project Structure
```
├───.vscode
├───artifacts
├───db
├───Event_Listener
│   ├───event_listener_spec
│   └───grpc_service
│       └───__pycache__
├───exporter
├───grafana
│   └───grafana
│       └───provisioning
│           └───datasources
├───grpc_service
└───prometheus
    └───prometheus
```
**db** 
<p>It hosts the mongodb instance.</p>

**Event_listner**
<p>It hosts the GRPC Server. It accepts the request for defined events and process the request and maintains the real-time stats of the network.</p>
<i>Create test data on stats-database</i>

```sh
docker exec -it stats-database_event_listener_1 sh -c "python grpc_service/test.py"
```

**exporter**
<p>It hosts the **mongodb-query-exporter**. It scraps the data from the mongodb and exposes the metrics. It works as intermediater between the prometheus and mongodb.</p>

**grafana**
<p>It hosts the grafana instance, which visualises the real-time stats of the nunet-network.</p>
<i>Create datasource and add the dashboard panels</i>

```sh
python3 grafana/script.py
```

**prometheus**
<p>It hosts a prometheus instance and scraps the data from the target and work as datasource for the grafana.</p>


